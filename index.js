$("#heartbeat").jRoll({
    radius: 70, 
    animation: "heartbeat",
    colors:['#D32F2F','#757575','#FFFFFF','#448AFF','#F2D03B']
});
$("#pulse").jRoll({
    radius: 70, 
    animation: "pulse",
    colors:['#D32F2F','#757575','#FFFFFF','#448AFF','#F2D03B']
});
$("#slicedspinner").jRoll({
    radius: 70, 
    animation: "slicedspinner"
});
$("#halfslicedspinner").jRoll({
    radius: 70, 
    animation: "halfslicedspinner"
});
$("#gyroscope").jRoll({
    radius: 70, 
    animation: "gyroscope"
});
$("#wave").jRoll({
    radius: 70, 
    animation: "wave"
});
$("#jumpdots").jRoll({
    radius: 70, 
    animation: "jumpdots"
});
$("#hordots").jRoll({
    radius: 70, 
    animation: "hordots"
});
$("#verdots").jRoll({
    radius: 70, 
    animation: "verdots"
});
$("#spreaddots").jRoll({
    radius: 70, 
    animation: "spreaddots"
});
$("#trailedspreaddots").jRoll({
    radius: 70, 
    animation: "trailedspreaddots"
});
$("#circledots").jRoll({
    radius: 70, 
    animation: "circledots"
});
$("#squares").jRoll({
    radius: 70, 
    animation: "squares"
});
$("#3Dsquares").jRoll({
    radius: 70, 
    animation: "3Dsquares"
});
$("#stackedsquares").jRoll({
    radius: 70, 
    animation: "stackedsquares"
});
$("#3dots").jRoll({
    radius: 70, 
    animation: "3dots"
});
$("#popdot").jRoll({
    radius: 70, 
    animation: "popdot"
});
$("#eq").jRoll({
    radius: 70, 
    animation: "eq"
});
$("#waterdrop").jRoll({
    radius: 70, 
    animation: "waterdrop"
});